use std::env;
use std::error::Error;
use std::io;
use std::io::Write;
use std::process::Command;
use std::str;

fn main() -> Result<(), Box<dyn Error>> {
    let rm_args: Vec<String> = env::args().collect();

    print!("\
rm {}
Sind Sie sicher? [j/n] ", &rm_args[1..].join(" "));
    io::stdout().flush()?;
    let mut bestaetigen = String::new();
    io::stdin().read_line(&mut bestaetigen)?;
    if bestaetigen.as_str() != "j\n" {
        println!("wird abgebrochen...");
        return Ok(());
    }

    let befehl = Command::new("rm").args(&rm_args[1..]).output()?;
    let befehl_out = str::from_utf8(&befehl.stdout)?;
    print!("{}", befehl_out);
    io::stdout().flush()?;
    let befehl_err = str::from_utf8(&befehl.stderr)?;
    eprint!("{}", befehl_err);
    io::stderr().flush()?;

    Ok(())
}
