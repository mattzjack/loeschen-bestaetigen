# löschen-bestätigen


```
$ cargo build --release

$ alias rm=pfad/zu/target/release/löschen-bestätigen
$ rm wichtige-datei.db
rm wichtige-datei.db
Sind Sie sicher? [j/n] n
wird abgebrochen...
```

Optionen `-i`, `-I`, & `--interactive` funktionieren nicht.
